import java.util.LinkedList;

public class Plateau {
    final int cote = 10;
    final int hauteur = 4;
    final int xyDepartNoir = 2;
    final int xyDepartBlanc = 7;
    final int horsTour = 3; //nombre de tour a partir duquel les seigneurs n'ont plus le droit d'etre sur leur tour
    private Case[][][] plateau;
    private int tour;

    private void beginGame(Joueur j) {
        for (Object pc : j.getPieces()) {
            Piece p = (Piece) pc;
            if(!p.inReserve()) {
                addPiece(p);
            }
        }
    }

    //constructeur par défaut
    public Plateau(Joueur j1, Joueur j2) {
        plateau = new Case[cote][cote][hauteur];
        tour = 1;
        // on met les coordonnées dans chaque case
        for (int x = 0; x < cote; x++) {
            for (int y = 0; y < cote; y++) {
                for (int z = 0; z < hauteur; z++) {
                    plateau[x][y][z] = new Case(x,y,z);
                }
            }
        }
        beginGame(j1);
        beginGame(j2);
    }


    //***************** ACCESSEURS *****************
    public Case getCase(int x, int y, int z) {
        Case c = null;
        if (isInPlateau(x, y, z)) {
            c = plateau[x][y][z];
        } else {
            System.out.println("case aux coordonnées (" + x + "," + y + "," + z + ") en dehors du plateau.");
        }
        return c;
    }

    public int getHauteur() {
        return hauteur;
    }

    public int getTour() {
        return tour;
    }

    public int getMinX() {
        return 0;
    }

    public int getMinY() {
        return 0;
    }

    public int getMinZ() {
        return 0;
    }

    public int getMaxX() {
        return cote - 1;
    }

    public int getMaxY() {
        return cote - 1;
    }

    public int getMaxZ() {
        return hauteur - 1;
    }

    //***************** MUTATEURS *****************
    public void setCase(Case c, int x, int y, int z) {
        if (isInPlateau(x, y, z)) {
            plateau[x][y][z] = c;
        } else {
            System.out.println("case aux coordonnées (" + x + "," + y + "," + z + ") en dehors du plateau.");
        }
    }

    public void tourSuivant() {
        tour++;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: delPiece
    goal: supprime une piece du plateau
    preconditions: la piece doit pouvoir se déplacer
    postconditions: none
    */
    public void delPiece(Piece p) {
        for (Object bl : p.getCompo()) {
            Bloc b = (Bloc) bl;
            Case c = this.getCase(b.getX(), b.getY(), b.getZ());
            c.delBloc();
        }
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: addPiece
    goal: ajoute une piece au plateau
    preconditions: la piece doit pouvoir aller à l'endroit où elle veut aller
    postconditions: none
    */
    public void addPiece(Piece p) {
        for (Object bl : p.getCompo()) {
            Bloc b = (Bloc) bl;
            Case c = this.getCase(b.getX(), b.getY(), b.getZ());
            c.addBloc(b);
        }

    }

    //***************** MÉTHODES *****************
    private Piece.couleurs oppositeClr(Piece.couleurs cl) {
        Piece.couleurs res;
        switch (cl) {
            case BLANC:
                res = Piece.couleurs.NOIR;
                break;

            case NOIR:
                res = Piece.couleurs.BLANC;
                break;

            default:
                res = Piece.couleurs.NC;
        }
        return res;
    }

    public boolean isInPlateau(int x, int y, int z) {
        return getMinX() <= x && getMinY() <= y && getMinZ() <= z && x <= getMaxX() && y <= getMaxY() && z <= getMaxZ();
    }

    public boolean estVide(int x, int y, int z) {
        boolean ret = true;
        if (isInPlateau(x, y, z)) {
            ret = plateau[x][y][z].getContenu() != Case.formes.VIDE;
        }
        return ret;
    }

    public boolean sgnrOnTwr(Piece.couleurs clSgnr, Piece.couleurs clTwr) {
        int xyTwr = ((clTwr == Piece.couleurs.NOIR) ? xyDepartNoir : xyDepartBlanc);
        //on récup la case où doit se trouver le seigneur si il y est
        Case c = getCase(xyTwr, xyTwr, hauteur - 1);
        boolean res = false;

        if (!c.estVide()) { //si la case n'est pas vide
            res = (c.getOnTop().getPiece().getCouleur() == clSgnr) &&
                    (c.getOnTop().getPiece().getName() == Piece.names.SEIGNEUR);
        }
        return res;
    }

    //renvoie vrai si le joueur de la couleur mentionnée à gagné
    public boolean victory(Piece.couleurs cl) {
        return ((this.getTour() >= horsTour && sgnrOnTwr(oppositeClr(cl), oppositeClr(cl))) || sgnrOnTwr(cl, oppositeClr(cl)));
    }

    //renvoie vrai si la partie est finie
    public boolean endGame() {
        return victory(Piece.couleurs.BLANC) || victory(Piece.couleurs.NOIR);
    }


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: makeTuple
    goal: transforme les deux éléments en un tuple
    preconditions: $<none>$
    postconditions: linkedList, doit agir comme tel
    */
    private LinkedList makeTuple(int x, int y) {
        LinkedList ret = new LinkedList();
        ret.add(y);
        ret.addFirst(x);
        return ret;
    }


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getVecteursMove
    goal: retourne une liste contenant les 4 vecteurs de déplacement horizontaux possibles
    preconditions: none
    postconditions: LinkedList de tuples
    */
    private LinkedList getVecteursMove() {
        LinkedList res = new LinkedList();
        res.add(makeTuple(1, 0));
        res.add(makeTuple(-1, 0));
        res.add(makeTuple(0, 1));
        res.add(makeTuple(0, -1));
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getVoisinsSeigneur
    goal: renvoie la première case "voisine" sur laquelle le seigneur peut s'arreter pour chaque direction
    preconditions: none
    postconditions: LinkedList de Case
    */
    public LinkedList getVoisinsSeigneur(Case c) {
        //on va raisonner en terme de vecteur de déplacement.
        //on s'arrete lorsqu'il n'est plus possible de se déplacer ou si on peut s'arreter sur la case
        LinkedList voisines = new LinkedList();
        LinkedList vecteur = getVecteursMove();
        for (Object vect : vecteur) {
            LinkedList v = (LinkedList) vect;
            int vx = (int) v.getFirst();
            int vy = (int) v.getLast();
            int x = c.getX() + vx;
            int y = c.getY() + vy;
            int z = c.getZ();
            boolean stop = !isInPlateau(x, y, z);
            while (!stop) {
                //on récupère la case qui est sur le même plan horizontal en suivant le vecteur
                Case cv = getCase(x, y, z);
                //on regarde ce qu'il y a comme type de forme dans cette case
                switch (cv.getContenu()) {
                    //si c'est vide
                    case VIDE:
                        //et qu'on est au sol
                        if (z == 0) {
                            //alors on peut s'y déplacer, donc on s'arrête la
                            voisines.add(cv);
                            stop = true;
                        } else { //sinon c'est ce qui se situe en dessous de cette case qui nous interresse
                            //donc on récupère la case en dessous
                            cv = getCase(x, y, z - 1);
                            //on regarde ce qu'elle contient
                            switch (cv.getContenu()) {
                                //si c'est un bloc
                                case CARRE:
                                    //il ne doit pas être un seigneur pour qu'on puisse se mettre dessus
                                    if (cv.getOnTop().getPiece().getName() != Piece.names.SEIGNEUR) {
                                        voisines.add(getCase(x,y,z));
                                    }
                                    //que la case soit bonne ou non on s'arrete la
                                    stop = true;
                                    break;
                                //si c'est un escalier qui descend
                                case TRIANGLE:
                                    //on doit regarder si il est bien orienté
                                    if (!cv.getBottom().getPiece().goodOrientation(vx, vy, -1)) {
                                        //sinon on s'arrete
                                        stop = true;
                                    } else {
                                        //si c'est bon on se déplace sur le triangle et on continue
                                        z--;
                                    }
                                    break;
                                //si c'est aucun des deux alors on s'arrete
                                default:
                                    stop = true;
                            }
                        }
                        break;
                    //si c'est un monte qui monte
                    case TRIANGLE:

                        //on doit regarder si il est bien orienté
                        if (!cv.getBottom().getPiece().goodOrientation(vx, vy, 0)) {
                            //si il ne l'est pas, on s'arrete
                            stop = true;
                        } else {
                            //sinon on positionne notre seigneur fictif sur la case au dessus
                            z++;
                        }
                        break;
                    default:
                        //si rien de tout ce qui est au dessus n'est respecté, alors on s'arrete
                        stop = true;
                }

                //une fois qu'on a check la case, et si on ne doit pas s'arreter
                if (!stop) {
                    x += vx;
                    y += vy;
                    //on vérifie que la prochaine case sera dans le plateau
                    stop = !isInPlateau(x, y, z);
                }
                //et c'est reparti pour un tour dans le while
            }
            //et c'est reparti pour un autre vecteur de déplacement
        }
        return voisines;
    }


    @Override
    public String toString() {
        String res = "";
        for (int x = 0; x < cote; x++) {
            for (int y = 0; y < cote; y++) {
                for (int z = 0; z < hauteur; z++) {
                    res = res + getCase(x,y,z).toString() + "\n";
                }
            }
        }
        return res;
    }


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: apply
    goal: joue les actions présentes dans la liste
    preconditions: les actions doivent être valides
    postconditions: none
    */
    public void apply (LinkedList<Action> lAct) {

    }

    /*
author: Mael Simon <simonmael@eisti.eu>
name: undo
goal: annule les actions présentes dans la liste
preconditions: les actions doivent avoir déjà été effectuées
postconditions: none
*/
    public void undo (LinkedList<Action> lAct) {

    }
}
