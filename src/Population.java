import groovy.json.internal.ArrayUtils;
import org.codehaus.groovy.runtime.ArrayUtil;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.Stream;

//population des individus
public class Population {
    final int arret = 100;
    final int save = 10;
    private int taille;
    final int defTaille = 100;
    final int div = 5; //nombre max de fois qu'un individu peut aparaitre dans une même population
    private Individu[] individus;

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: Population
    goal: crée une population d'individus
    preconditions: none
    postconditions: nouvelle population
    */
    public Population() {
        taille = defTaille;
        individus = new Individu[taille];
        for (int i = 0; i < taille; i ++) {
            System.out.println("creation individu "+i);
            individus[i] = new Individu();
        }
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: Population
    goal: charge une population d'individus à partir du fichier f
    preconditions: f permet de charger une population d'individus
    postconditions: population chargée
    */
    public Population(String f) {
        // TODO: 29/05/17 pour pouvoir charger à partir d'un fichier 
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: Population
    goal: crée une population d'individus à partir des données en paramètres
    preconditions: none
    postconditions: nouvelle population qui correspond aux paramètres en entrée
    */
    public Population(int t, Individu[] pop) {
        taille = t;
        individus = pop;
    }


    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public Individu[] getIndividus() {
        return individus;
    }

    public void setIndividus(Individu[] individus) {
        this.individus = individus;
    }


    private void makePrc (Individu[] inds) {
        //tableau des pourcentages
        int nb = inds.length;
        for (int i = 0; i < nb; i++) {
            inds[i].setProba((i + 1.0)/(nb*(nb+1.0)/2.0));
        } //probabilité pour chacun d'être choisit
    }

    //on considère que tous les individus st répartis proportionnellement sur un cercle.
    //on imagine alors qu'on fait tourner ce cercle, et qu'il s'arrete sur un individu (la roulette)
    //c'est cet individu qui sera renvoyé
    private Individu roulette (Individu[] inds) {
        double r = Math.random(); //r va déterminer quel individu on va prendre pour ce tour
        int c = -1;
        double tot = 0;
        int nb = inds.length;
        for (int k = 0; k < nb; k++) {
            tot += this.getIndividus()[k].getProba(); //on a le prctage de tous ceux qui sont en dessous
            if (c == -1 && r < tot) {c = k;} //si c'est la première fois que r est en dessous du prctg, c'est qu'il est dans le rayon correspondant à la proba de l'individu k
        }
        return inds[c];
    }

    //tri du plus petit au plus grand suivant le score
    private void sort() {
        Arrays.sort(this.getIndividus(), new Comparator<Individu>() {
            @Override
            public int compare(Individu i1, Individu i2) {
                return i2.getScore() - i1.getScore(); //on veut que ça soit trié du plus petit au plus grand
            }
        });
    }

    private Population selection() {
        //on tri les individus par ordre décroissant

        makePrc(this.getIndividus()); //on définit leur pourcentage

        int t = this.getTaille() / 2; //on divise la taille de la population par 2
        //comme ça on a moitié parents qui viennent de la génération d'avant, et moitié enfants qui vont être créés
        Individu[] ind = new Individu[t];
        sort();

        //on va faire une sélection proportionnelle au rang

        for (int i = 0; i < t; i++) {
            //ind[i] = new Individu(this.getIndividus()[t - i]);
            ind[i] = new Individu(roulette(this.getIndividus()));
        }
        Population p = new Population(t,ind);
        return p;
    }

    private Population reproduction() {
        makePrc(this.getIndividus()); //on refait leur prctage de chance de se reproduire en fonction des individus restants
        Individu[] children = new Individu[this.getTaille()]; //on fait autant d'enfants qu'il y a de parents
        int round = this.getTaille() / 2;

        for (int i = 0; i < round; i++) {
            Individu i1 = roulette(this.getIndividus());
            Individu i2 = i1;
            while (i1 == i2) {
                i2 = roulette(this.getIndividus());
            }
            Individu[] twoChild = i1.reproduction(i2);
            children[i * 2] = twoChild[0];
            children[i * 2 + 1] = twoChild[1];
        }
        if (round * 2 != children.length) {children[children.length - 1] = new Individu();} //si il manque un individu on le crée aléatoirement
        return this;
    }

    private void mutation() {
        for (Individu i : this.getIndividus()) {
            i.mutation();
        }
    }

    // TODO: 29/05/17 évaluation à faire totalement
    private void fitness() {
        int moy = 0;
        int best = -1;
        for (Individu i : individus) {
            int sum = 0;
            for (int s:i.getPoidsPieces()) {sum += s;}
            sum += i.getCoefDistanceTour() + i.getCoefDistancePoss();
            i.setScore(sum);
            moy += i.getScore();
            if (best == -1 || i.getScore() > best) {best = i.getScore();}
        }
        System.out.println("score moyen : "+moy/this.getIndividus().length);
        System.out.println("meilleur : "+best);
    }

    private Population fusion(Population p1, Population p2) {
        p1.setTaille(p1.getTaille() + p2.getTaille()); //on additionne la taille des deux populations
        Individu[] ind = new Individu[p1.getTaille()];
        Individu[] i1 = p1.getIndividus();
        Individu[] i2 = p2.getIndividus();
        for (int i = 0; i < i1.length; i++) {ind[i] = i1[i];}
        for (int i = i1.length; i < p1.getTaille(); i++) {ind[i] = i2[i - i1.length];}
        p1.setIndividus(ind);
        ind = null;
        p2 = null;
        return p1;
    }

    //fonction qui ne sert que d'information, lance une alerte si la diversité vient à trop diminuer
    private void diversite () {
        //on va se servir du score pour stocker le nombre de fois qu'un individu est présent
        boolean alert = false;
        for (Individu i:getIndividus()) {
            i.setScore(0);
        }
        for (Individu i:getIndividus()) {
            i.setScore(i.getScore() - 1);
            alert = alert || (i.getScore() <= -div);
        }
        if (alert) {System.out.println("PROBLEME DE DIVERSITE AU MOINS UN INDIVIDU SURREPRESENTE!");}
    }

    public Individu evolution() {
        int generations = 0;
        Population p = this;
        while (generations < arret) {
            System.out.println("\ngénération : "+generations);
            p.fitness();

            Population parents = p.selection();
            Population enfants = parents.reproduction();
            enfants.mutation();

            //sauvegarde de la population
            if (Math.floorMod(generations,save) == 0) {
                //System.out.println(Arrays.asList(parents.getIndividus()).toString()); //permet d'afficher la population actuelle
                System.out.println("save");
                // TODO: 29/05/17 sauvegarde à créer 
            }
            p = fusion(parents,enfants);
            //parents = null;
            //enfants = null;
            generations ++;
        }
        // TODO: 29/05/17 fonction pour avoir celui avec le meilleur score
        p.sort();
        return p.getIndividus()[defTaille - 1]; //on renvoie celui qui à le plus haut score
    }
}
