/*
un bloc est une sous partie d'une pièce, dont la taille correspond à une case du plateau.
Un bloc est donc soit un cube, soit un prisme (de manière à former les escaliers)
 */
public class Bloc {
    public enum formes {CARRE, TRIANGLE, NC}
    private formes forme;
    private int x;
    private int y;
    private int z;
    private Piece piece;


    //constructeur par défaut
    public Bloc() {
        forme = formes.NC;
        x = 0;
        y = 0;
        z = 0;
        piece = new Piece();
    }

    //constructeur, qui ne doit être utilisé que lors de la création du jeu
    public Bloc(formes f, int xc, int yc, int zc, Piece p) {
        if (f != formes.NC) {
            forme = f;
            x = xc;
            y = yc;
            z = zc;
            piece = p;
        }
    }

    //***************** ACCESSEURS *****************
    public formes getForme() {
        return forme;
    }

    public Piece getPiece() {return piece;}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: isSurrounded
    goal: retourne vrai si le bloc est entourée (sur le plan horizontal) dans le sens où on ne peut pas le déplacer
    preconditions: doit etre dans le plateau, et pas sur un bord du plateau
    postconditions: boolean
    */
    public boolean isSurrounded (Plateau pl) {
        int x = this.getX();
        int y = this.getY();
        int z = this.getZ();
        return  (pl.getCase(x - 1,y,z).getContenu() != Case.formes.VIDE || pl.getCase(x - 1,y,z).getOnTop().getPiece().getName() != Piece.names.SEIGNEUR)&&
                (pl.getCase(x + 1,y,z).getContenu() != Case.formes.VIDE || pl.getCase(x + 1,y,z).getOnTop().getPiece().getName() != Piece.names.SEIGNEUR)&&
                (pl.getCase(x,y - 1,z).getContenu() != Case.formes.VIDE || pl.getCase(x,y - 1,z).getOnTop().getPiece().getName() != Piece.names.SEIGNEUR)&&
                (pl.getCase(x,y + 1,z).getContenu() != Case.formes.VIDE || pl.getCase(x,y + 1,z).getOnTop().getPiece().getName() != Piece.names.SEIGNEUR);
    }

    //***************** MUTATEURS *****************


    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
