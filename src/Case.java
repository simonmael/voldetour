import java.util.LinkedList;

public class Case {
    //une case est : soit vide, soit avec un cube dedans, soit avec 1 OU 2 triangles dedans
    //donc renvoie qqch qui est soit vide, soit un bloc, soit 2 blocs
    private LinkedList blocs ;
    public enum formes {VIDE, CARRE, TRIANGLE}
    private formes contenu;
    int x,y,z; //coordonnées
    //pointeurs en java: objet directement. Qd on dit prend la valeur, fait pas copie, passe pointeur

    //constructeur par défaut
    public Case() {
        contenu = formes.VIDE;
    }

    //constructeur de cases
    public Case(int xp, int yp, int zp) {
        contenu = formes.VIDE;
        x = xp;
        y = yp;
        z = zp;
        blocs = new LinkedList();
    }


    //***************** ACCESSEURS *****************
    public formes getContenu() {
        return contenu;
    }

    public LinkedList getBlocs() {
        return  blocs;
    }

    public int getNbPiece() {
        return blocs.size();
    }

    public Bloc getOnTop() {
        if (getNbPiece() == 0) {
            System.out.println("Il n'y a pas de blocs dans cette case");
        }
        return (Bloc)blocs.getLast();
    }

    public Bloc getBottom() {
        if (getNbPiece() == 0) {
            System.out.println("Il n'y a pas de blocs dans cette case");
        }
        return (Bloc)blocs.getFirst();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public boolean estVide() {return getContenu() == formes.VIDE;}

    //***************** MUTATEURS *****************
    //pour ajouter un bloc dans une case
    public void addBloc(Bloc blc) {
        switch (contenu) {
            case CARRE:
                System.out.println("ERROR TRY TO ADD A BLOCK IN A CASE THAT ALLREADY CONTAIN A CUBE ");
                break;
            case TRIANGLE:
                if (blc.getForme() == Bloc.formes.TRIANGLE) { //ne gère pas encore l'orientation !!
                    contenu =formes.CARRE;
                    blocs.add(blc);
                }
                break;
            default: //si c'est vide
                if (blc.getForme() == Bloc.formes.CARRE) {
                    contenu = formes.CARRE;
                } else {
                    contenu = formes.TRIANGLE;
                }
                blocs.add(blc);
        }
    }

    //pour enlever un bloc d'une case
    public  void delBloc() {
        switch (contenu) {
            case TRIANGLE:
                contenu = formes.VIDE;
                blocs.removeLast();
                break;
            case CARRE:
                Bloc objRemove = (Bloc)blocs.removeLast(); //on récupère l'objet qu'on vient d'enlever
                if (objRemove.getForme() == Bloc.formes.TRIANGLE) { //si l'objet qu'on enlève est un triangle ça veut dire qu'il y avait deux triangles dans la même case
                    contenu = formes.TRIANGLE;
                } else {
                    contenu= formes.VIDE;
                }
                break;
            default:
                System.out.println("ERROR TRY TO DELETE A BLOCK IN A CASE THAT CONTAIN ANY ");
        }
    }

    @Override
    public String toString() {
        String t = (getContenu() == formes.VIDE) ? "" : ":" +getOnTop().getPiece().getName().toString() ;
        return  "["+ x +"]["+ y +"]["+ z +"]" + getContenu().toString() + t;
    }
}
