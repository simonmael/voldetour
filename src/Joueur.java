import sun.awt.image.ImageWatched;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Joueur {
    private Piece.couleurs couleur;
    private String name;
    private LinkedList pieces;

    public Joueur (){
        couleur = Piece.couleurs.NC;
        name = "unknown";
        pieces = null;
    }


    //crée la liste de toutes les pièces du joueur.

    private LinkedList CreateAllPiece (Piece.couleurs cl){
        LinkedList res = new LinkedList();
        res.add(new Piece(Piece.names.TOUR, cl, 3));
        res.add(new Piece(Piece.names.SEIGNEUR, cl, 3));
        res.add(new Piece(Piece.names.BLOC, cl, 3));
        res.add(new Piece(Piece.names.ESCALIER, cl, 3));
        res.add(new Piece(Piece.names.BLOC, cl, 2));
        res.add(new Piece(Piece.names.BLOC, cl, 2));
        res.add(new Piece(Piece.names.ESCALIER, cl, 2));
        res.add(new Piece(Piece.names.ESCALIER, cl, 2));
        res.add(new Piece(Piece.names.BLOC, cl, 1));
        res.add(new Piece(Piece.names.BLOC, cl, 1));
        res.add(new Piece(Piece.names.ESCALIER, cl, 1));
        res.add(new Piece(Piece.names.ESCALIER, cl, 1));
        return res;
    }

    public Joueur (Piece.couleurs cl, String n) {
        couleur = cl;
        name = n;
        pieces = CreateAllPiece(cl);
    }

    //***************** ACCESSEURS *****************

    public Piece.couleurs getCouleur() {
        return couleur;
    }

    public String getName() {
        return name;
    }

    public LinkedList getPieces() {
        return pieces;
    }

    //renvoie la liste des pieces en reserves d'un joueur
    public  LinkedList inReserve() {
        LinkedList res = new LinkedList();
        for (Piece element : (Iterable<Piece>) pieces) {
            if (element.inReserve()) {
                res.add(element);
            }
        }
        return res;
    }

    //renvoie la liste des pieces sur le terrain d'un joueur
    public  LinkedList onTerrain() {
        LinkedList res = new LinkedList();
        for (Piece element : (Iterable<Piece>) pieces) {
            if (!element.inReserve()) {
                res.add(element);
            }
        }
        return res;
    }

    //renvoie la liste des blocs d'un joueur
    public  LinkedList allBlocs() {
        LinkedList res = new LinkedList();
        for (Piece element : (Iterable<Piece>) pieces) {
            if (element.getName() == Piece.names.BLOC) {
                res.add(element);
            }
        }
        return res;
    }

    //renvoie la liste des escaliers d'un joueur
    public  LinkedList allEscaliers() {
        LinkedList res = new LinkedList();
        for (Piece element : (Iterable<Piece>) pieces) {
            if (element.getName() == Piece.names.ESCALIER) {
                res.add(element);
            }
        }
        return res;
    }

    //renvoie la liste des pièces qui peuvent bouger d'un joueur
    public  LinkedList allCanMove(Plateau p) {
        LinkedList res = new LinkedList();
        for (Piece element : (Iterable<Piece>) pieces) {
            if (element.canMove(p) == 0) {
                res.add(element);
            }
        }
        return res;
    }



    public String toString() {
        return this.getName() + " " + this.getPieces().toString() + " \n";
    }

    public LinkedList getPiece(Piece.names n, int taille) {
        LinkedList ret = new LinkedList();
        for (Object pc : this.getPieces()) {
            Piece p = (Piece) pc;
            if (p.getTaille() == taille && p.getName() == n) {
                ret.add(p);
            }
        }
        return ret;
    }

    private boolean finPossible (boolean e, boolean b, boolean s) {
        return e || s || b;
    }


    private LinkedList inCommun (LinkedList l1, LinkedList l2) {
        LinkedList res = new LinkedList();
        for (Object pc : l1) {
            if (l2.contains(pc)) {res.add(pc);}
        }
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getDispos
    goal: renvoie la liste des pièces que l'on peut bouger
    preconditions: none
    postconditions: liste de pièces
    */
    public LinkedList<Piece> getDispos(Plateau p) {
        LinkedList<Piece> ret = new LinkedList<Piece>();
        for (Object o : this.getPieces()) {
            Piece pc = (Piece)o;
            if (pc.inReserve() || (pc.canMove(p) == 0)) {ret.add(pc);}
        }
        return ret;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getCoinces
    goal: renvoie la liste des pièces que l'on ne peut pas bouger
    preconditions: none
    postconditions: liste de pièces
    */
    public LinkedList<Piece> getCoinces(Plateau p) {
        LinkedList<Piece> ret = new LinkedList<Piece>();
        for (Object o : this.getPieces()) {
            Piece pc = (Piece)o;
            if (pc.canMove(p) != 0) {ret.add(pc);}
        }
        return ret;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getImuns
    goal: renvoie la liste des pièces qui sont immunisées, cad qu'on ne peut pas bloquer
    preconditions: l doit etre la liste des pièces disponibles, cad qu'on peut bouger
    postconditions: liste de pièces
    */
    public LinkedList<Piece> getImuns(Plateau p, LinkedList<Piece> l) {
        LinkedList<Piece> ret = new LinkedList<Piece>();
        for (Piece pc : l) {
            int x = pc.getX();
            int y = pc.getY();
            int z = pc.getHigherZ();
            if (pc.inReserve() || (p.getCase(x,y,z).getOnTop().getPiece() == pc && z == p.getHauteur() - 1)) {ret.add(pc);}
        }
        return ret;
    }


    private Piece choixPiece(Piece.names f,Plateau p) {
        LinkedList l;
        switch (f) {
            case ESCALIER:
                l = inCommun(this.allEscaliers(),this.allCanMove(p));
                break;
            case BLOC:
                l = inCommun(this.allBlocs(),this.allCanMove(p));
                break;
            default:
                return (Piece)this.getPiece(Piece.names.SEIGNEUR,1).getFirst();
        }
        int i = 0;
        for (Object pce : l) {
            Piece pc = (Piece)pce;
            System.out.println(i + pc.toString() + "\n");
            i++;
        }
        Scanner keyboard = new Scanner(System.in);
        int choix = keyboard.nextInt();
        return (Piece)l.toArray()[choix];
    }

    public void tour (Plateau plt, Joueur adv, boolean IA) {
        if (!IA) {
            Scanner keyboard = new Scanner(System.in);
            boolean moveEsc = false, moveBlc = false, moveSgn = false, fin = false;
            while (!fin) {
                System.out.println(this.getName());
                System.out.println("Actions possibles");
                System.out.println((moveEsc) ? "vous ne pouvez plus bouger d'escalier" : "1 : bouger escalier" );
                System.out.println((moveBlc) ?  "vous ne pouvez plus bouger de bloc" : "2 : bouger bloc" );
                System.out.println((moveSgn) ? "vous ne pouvez plus bouger le seigneur" : "3 : bouger seigneur");
                System.out.println((finPossible(moveEsc, moveBlc, moveSgn)) ? "4 : fin du tour " : "vous devez effectuer au moins une action");
                int choix = keyboard.nextInt();
                int err = 0;
                Piece p;
                switch (choix) {
                    case 1:
                        p = this.choixPiece(Piece.names.ESCALIER,plt);
                        err = p.askMove(plt);
                        if (err == 0) {moveEsc = true;}
                        break;
                    case 2:
                        p = this.choixPiece(Piece.names.BLOC,plt);
                        err = p.askMove(plt);
                        if (err == 0) {moveBlc = true;}
                        break;
                    case 3:
                        p = this.choixPiece(Piece.names.SEIGNEUR,plt);
                        err = p.askMove(plt);
                        if (err == 0) {
                            moveSgn = true;
                            fin = true;
                        }
                        break;
                    default:
                        fin = finPossible(moveEsc,moveBlc,moveSgn);
                }
                System.out.println(plt.toString());
            }
        } else {
            /*
            IA ia = new IA("fichier1.txt");
            LinkedList actions = ia.getActions(plt,this,adv);
            for (Object action : actions) {
                Action act = (Action)action;
                //int err = plt.apply(act);
                int err = 0;
                if (err != 0) {
                    System.out.println("ALERT IA FAIT DES CHOSES PAS LÉGALES");
                }
            }
            */
        }
    }
}
