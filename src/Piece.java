import java.util.LinkedList;
import java.util.Scanner;


/*
une piece est un ensemble de 1 ou plusieurs blocs, qui définit une pièce du jeu.
Elle donne notament la couleur de la pièce, sa taille, son orientation, si elle est en reserve ou non, etc
*/
public class Piece {
    final int xyDepartNoir = 2;
    final int xyDepartBlanc = 7;
    final int xyzDefaut = 0;
    final int tailleSeigneur = 1;
    final int tailleTour = 3;
    final int tailleMini = 1;
    final int tailleMaxi = 3;
    final int maxMove = 3;

    public enum names {TOUR, SEIGNEUR, BLOC, ESCALIER}

    public enum orientationLacet {NORD, SUD, EST, OUEST}

    public enum orientationTangage {HAUT, BAS, HORIZONTAL} //mêmes axes de rotation qu'en aéronautique.

    public enum couleurs {BLANC, NOIR, NC}

    private names name;
    private couleurs couleur;
    private int taille;
    private boolean enReserve;
    private orientationTangage tangage;
    private int roulis; //0, 90, 180, 270
    private orientationLacet lacet;
    private LinkedList<Bloc> assemblage;
    private int x;
    private int y;
    private int z;
    private int numero; //le numéro auquel est stocké son poids dans le génome

    //constructeur par défaut
    public Piece() {
        name = names.BLOC;
        couleur = couleurs.NC;
        taille = 0;
        enReserve = true;
        tangage = orientationTangage.HAUT;
        roulis = 0;
        lacet = orientationLacet.NORD;
        x = xyzDefaut;
        y = xyzDefaut;
        z = xyzDefaut;
        numero = 0;
    }

    //constructeur, qui ne doit être utilisé que lors de la création du jeu
    //names type de la piece, cl couleur de la piece, t sa taille
    //par défaut, a part pour la tour et le seigneur, les pièces seront en réserve
    //a savoir que la partie "pentue" des escaliers est le dernier élément de la liste
    public Piece(names nm, couleurs cl, int t) {
        if (cl != couleurs.NC && t >= tailleMini && t <= tailleMaxi) { //si la pièce est une pièce qui existe vraiment dans le jeu
            name = nm;
            couleur = cl;
            tangage = orientationTangage.HAUT;
            roulis = 0;
            lacet = orientationLacet.NORD;
            switch (nm) {
                case SEIGNEUR:
                    taille = tailleSeigneur;
                    enReserve = false;
                    if (cl == couleurs.NOIR) {
                        x = xyDepartNoir;
                        y = xyDepartNoir;
                    } else {
                        x = xyDepartBlanc;
                        y = xyDepartBlanc;
                    }
                    z = 3;
                    assemblage = makeStructure(taille, nm, x, y, z, this);
                    break;

                case TOUR:
                    taille = tailleTour;
                    enReserve = false;
                    if (cl == couleurs.NOIR) {
                        x = xyDepartNoir;
                        y = xyDepartNoir;
                    } else {
                        x = xyDepartBlanc;
                        y = xyDepartBlanc;
                    }
                    z = 0;
                    assemblage = makeStructure(taille, nm, x, y, z, this);
                    break;

                default:
                    taille = t;
                    enReserve = true;
                    x = xyzDefaut;
                    y = xyzDefaut;
                    z = xyzDefaut;
                    assemblage = makeStructure(taille, nm, x, y, z, this);
                    numero = (nm == names.BLOC) ? 4 - taille : 7 - taille;
            }
        } else {
            System.out.println("ERROR WHILE CREATING THE PIECE");
        }
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: makeStructure
    goal: crée la structure d'une pièce (ensemble des blocs qui la compose)
    preconditions: pièce qui existe dans le jeu voldetour
    postconditions: liste contenant tous les blocs, la partie "pentue" des escaliers est le dernier élément de la liste
    */
    private LinkedList makeStructure(int taille, names n, int x, int y, int z, Piece p) {
        LinkedList l = new LinkedList();
        for (int i = 0; i < taille - 1; i++) {
            Bloc b = new Bloc(Bloc.formes.CARRE, x, y, z + i, p);
            l.add(b);
        }
        if (n == names.ESCALIER) {
            Bloc b = new Bloc(Bloc.formes.TRIANGLE, x, y, z + taille - 1, p);
            l.add(b);
        } else {
            Bloc b = new Bloc(Bloc.formes.CARRE, x, y, z + taille - 1, p);
            l.add(b);
        }
        return l;
    }

    //***************** ACCESSEURS *****************
    public names getName() {
        return name;
    }

    public couleurs getCouleur() {
        return couleur;
    }

    public int getTaille() {
        return taille;
    }

    public boolean inReserve() {
        return enReserve;
    }

    public orientationTangage getTangage() {
        return tangage;
    }

    public int getRoulis() {
        return roulis;
    }

    public orientationLacet getLacet() {
        return lacet;
    }

    public LinkedList<Bloc> getCompo() {
        return assemblage;
    }

    public String toString() {
        String retour = "";
        retour += "nom : " + name + "\n";
        retour += "couleur : " + couleur + "\n";
        retour += "taille : " + taille + "\n";
        retour += "en reserve : " + enReserve + "\n";
        retour += "tangage : " + tangage + "\n";
        retour += "roulis : " + roulis + "\n";
        retour += "lacet : " + lacet + "\n";
        //retour += "composition : " + assemblage + "\n";
        retour += "coordonnées : (" + x + "," + y + "," + z + ")\n";
        return retour;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    //renvoie le X le plus haut
    public int getHigherX() {
        int high = this.getX();
        for (Bloc b : this.getCompo()) {
            if (b.getX() > high) {
                high = b.getX();
            }
        }
        return high;
    }

    //renvoie le X le plus bas
    public int getLowerX() {
        int low = this.getX();
        for (Bloc b : this.getCompo()) {
            if (b.getX() < low) {
                low = b.getX();
            }
        }
        return low;
    }

    //renvoie le Y le plus haut
    public int getHigherY() {
        int high = this.getY();
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            if (b.getY() > high) {
                high = b.getY();
            }
        }
        return high;
    }

    //renvoie le Y le plus bas
    public int getLowerY() {
        int low = this.getY();
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            if (b.getY() < low) {
                low = b.getY();
            }
        }
        return low;
    }

    //renvoie le Z le plus haut
    public int getHigherZ() {
        int high = this.getZ();
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            if (b.getZ() > high) {
                high = b.getZ();
            }
        }
        return high;
    }

    //renvoie le Z le plus bas
    public int getLowerZ() {
        int low = this.getZ();
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            if (b.getZ() < low) {
                low = b.getZ();
            }
        }
        return low;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getPente
    goal: retourne la partie pentue des escaliers
    preconditions: la piece doit etre un escalier
    postconditions: le bloc qui est la pente de l'escalier
    */
    public Bloc getPente() {
        return (Bloc) this.getCompo().getLast();
    }

    public int getNumero() {
        return numero;
    }

    //***************** MUTATEURS *****************

    private int lacetToRoulis(orientationLacet l) {
        int retour;
        switch (l) {
            case EST:
                retour = 90;
                break;
            case SUD:
                retour = 180;
                break;
            case OUEST:
                retour = 270;
                break;
            default:
                retour = 0;
        }
        return retour;
    }

    private orientationLacet roulisToLacet(int r) {
        orientationLacet l;
        switch (r) {
            case 90:
                l = orientationLacet.EST;
                break;
            case 180:
                l = orientationLacet.SUD;
                break;
            case 270:
                l = orientationLacet.OUEST;
                break;
            default:
                l = orientationLacet.NORD;
        }
        return l;
    }

    //fait pivoter la pièce autour de l'axe de tangage
    public void setTangage(orientationTangage tang) {
        if (tang != tangage) {
            int var = 0; //indique de combien on bouge
            int modif; //indique dans quel sens on bouge
            switch (tang) {
                case HORIZONTAL:
                    if (roulis == 0 || roulis == 90) {
                        modif = 1;
                    } else {
                        modif = -1;
                    }
                    for (Object bl : this.getCompo()) {
                        Bloc b = (Bloc) bl;
                        b.setZ(this.z);
                        if (roulis == 0 || roulis == 180) {
                            b.setY(b.getY() + var);
                        } else {
                            b.setY(b.getY() + var);
                        }
                        var += modif;
                    }
                    lacet = roulisToLacet(roulis);
                    roulis = 0;
                    break;

                case HAUT:
                    if (tangage == orientationTangage.BAS) {
                        for (Object bl : this.getCompo()) {
                            Bloc b = (Bloc) bl;
                            b.setZ(this.z + var);
                            var++;
                        }
                    } else {
                        if (lacet == orientationLacet.NORD || lacet == orientationLacet.EST) {
                            modif = -1;
                        } else {
                            modif = 1;
                        }
                        for (Object bl : this.getCompo()) {
                            Bloc b = (Bloc) bl;
                            b.setZ(this.z + Math.abs(var));
                            if (lacet == orientationLacet.NORD || lacet == orientationLacet.SUD) {
                                b.setY(b.getY() + var);
                            } else {
                                b.setY(b.getY() + var);
                            }
                            var += modif;
                        }
                        roulis = lacetToRoulis(lacet);
                        lacet = orientationLacet.NORD;
                    }
                    break;

                case BAS:
                    if (tangage == orientationTangage.HAUT) {
                        for (Object bl : this.getCompo()) {
                            Bloc b = (Bloc) bl;
                            b.setZ(this.z + var);
                            var--;
                        }
                    } else {
                        if (lacet == orientationLacet.NORD || lacet == orientationLacet.EST) {
                            modif = -1;
                        } else {
                            modif = 1;
                        }
                        for (Object bl : this.getCompo()) {
                            Bloc b = (Bloc) bl;
                            b.setZ(this.z - Math.abs(var));
                            if (lacet == orientationLacet.NORD || lacet == orientationLacet.SUD) {
                                b.setY(b.getY() + var);
                            } else {
                                b.setY(b.getY() + var);
                            }
                            var += modif;
                        }
                        roulis = lacetToRoulis(lacet);
                        lacet = orientationLacet.NORD;
                    }

            }
            tangage = tang;
        }
    }

    public void setRoulis(int roul) {
        if ((tangage != orientationTangage.HORIZONTAL) && (roul == 0 || roul == 90 || roul == 180 || roul == 270) ||
                (tangage == orientationTangage.HORIZONTAL) && (roul == 0 || roul == 180)) {
            roulis = roul;
        } else {
            System.out.println("ERROR " + roul + " IS NOT A VALID ROULIS WITH " + tangage + " AS TANGAGE");
        }
    }

    private void appliqueModifLacet(LinkedList assemblage, orientationLacet lacet, int modif) {
        int var = 0;
        for (Object bl : assemblage) {
            Bloc b = (Bloc) bl;
            if (lacet == orientationLacet.NORD || lacet == orientationLacet.SUD) {
                b.setY(b.getY() + var);
            } else {
                b.setX(b.getX() + var);
            }
            var += modif;
        }
    }

    public void setLacet(orientationLacet lac) {
        if (tangage == orientationTangage.HORIZONTAL) {
            int modif;
            //d'abord on remet tous les blocs sur la case de base
            if (lacet == orientationLacet.NORD || lacet == orientationLacet.EST) {
                modif = -1;
            } else {
                modif = 1;
            }
            appliqueModifLacet(this.getCompo(), lacet, modif);
            //puis on change le lacet
            lacet = lac;
            //et on réétends les blocs
            if (lacet == orientationLacet.NORD || lacet == orientationLacet.EST) {
                modif = 1;
            } else {
                modif = -1;
            }
            appliqueModifLacet(this.getCompo(), lac, modif);
        } else {
            System.out.println("ERROR TRY TO SET LACET WHILE PIECE IS NOT HORIZONTAL");
        }
    }

    public void setX(int x) {
        int move = x - this.x;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            b.setX(b.getX() + move);
        }
        this.x = x;
    }

    public void setY(int y) {
        int move = y - this.y;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            b.setY(b.getY() + move);
        }
        this.y = y;
    }

    public void setZ(int z) {
        int move = z - this.z;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            b.setZ(b.getZ() + move);
        }
        this.z = z;
    }

    //***************** MÉTHODES *****************

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getAngle
    goal: retourne un int correspondant à une rotation des escaliers. Le nombre ne veut rien dire en soit, il s'agit juste d'une convention.
          on considère que le nombre est positif si l'escalier a une de ses faces qui touche "le sol", négatif si c'est son complémentaire pour former un bloc
    preconditions: la pièce doit être un escalier, n'a pas de sens sinon.
    postconditions: int correspondant à une convention
    */
    private int getAngle() {
        //déterminer si escalier pour monter ou complémentaire
        int sens = (this.tangage == orientationTangage.BAS || this.tangage == orientationTangage.HORIZONTAL && this.roulis == 180) ? -1 : 1;
        //sens de la pente
        int rls = (this.tangage == orientationTangage.HORIZONTAL) ? lacetToRoulis(this.getLacet()) : this.getRoulis() ;
        return sens * Math.floorMod(rls, 90);
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: canFitTogether
    goal: renvoie vrai si les deux pièces peuvent s'emboiter
    preconditions: doivent etre des escaliers
    postconditions: boolean
    */
    public boolean canFitTogether(Piece pce) {
        return (this.getAngle() + pce.getAngle() == 0);
    }

    //renvoie vrai si la pièce est dans le plateau
    private boolean inPlateau(Plateau pl) {
        boolean res = true;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            //check pour tous les blocs s'ils sont dans le plateau
            res = res && pl.isInPlateau(b.getY(), b.getY(), b.getZ());
        }
        return res;
    }

    //renvoie vrai si une partie de la pièce est au dessus de la tour
    private boolean isOverTower() {
        return (this.getHigherZ() > (tailleTour - 1)); //on fait -1 car on commence à 0
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: voidUnder
    goal: retourne vrai si il y a du vide sous la pièce. Sert quand on veut déplacer la pièce à un endroit
    preconditions: doit être dans le plateau
    postconditions: boolean
    */
    private boolean voidUnder(Plateau pl) {
        boolean res = true;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc)bl;
            int x = b.getX(), y = b.getY(), z = b.getZ();
            int lz = this.getLowerZ();
            //on regarde pour tous les blocs qui sont le plus bas si il y a un carré dans la case en dessous
            if (pl.isInPlateau(x,y,z-1) && (z == lz)) {res = res && pl.getCase(x,y,z-1).getContenu() == Case.formes.CARRE;}
            //si le bloc est un escalier orienté vers le bas, on vérifie que la case ou il est contient deja un escalier
            //(on ne vérifie pas l'emboitement ici)
            if ((z == lz) && b.getForme() == Bloc.formes.TRIANGLE && this.getTangage() == orientationTangage.BAS) {res = res && pl.getCase(x,y,z).getContenu() == Case.formes.TRIANGLE;}
        }
        //du coup si toutes les conditions énoncés dessus sont bonnes, alors la pièce respecte les règles (res = true)
        //donc on l'inverse pour dire qu'il n'y a pas de vide en dessous
        return !res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: layerOver
    goal: renvoie vrai si il y a une piece strictement au dessus
    preconditions: doit etre dans le plateau
    postconditions: boolean
    */
    private boolean layerOver(Plateau pl) {
        boolean res = true;
        for (Object bl : this.getCompo()) {
            Bloc b = (Bloc) bl;
            int x = b.getX(), y = b.getY(), z = b.getZ();
            int hz = this.getHigherZ();
            //on regarde pour tous les blocs qui sont le plus haut si il n'y a rien dans la case au dessus
            if (pl.isInPlateau(x, y, z + 1) && (z == hz)) {
                res = res && pl.getCase(x, y, z + 1).estVide();
            }
        }
        //du coup si toutes les conditions énoncés dessus sont bonnes, alors la pièce respecte les règles (res = true)
        //donc on l'inverse pour dire qu'il n'y a pas de vide en dessous
        return !res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: isEmboitee
    goal: renvoie vrai si une piece est emboitée dans celle-ci
    preconditions: doit etre dans le terrain
    postconditions: boolean
    */
    private boolean isEmboitee(Plateau pl) {
        boolean res = false;
        if (this.getName() == names.ESCALIER) {
            Bloc pente = this.getPente(); //on recupère la pente de l'escalier
            Case c_pente = pl.getCase(pente.getX(), pente.getY(), pente.getZ()); //on prends la case où elle est
            res = c_pente.getNbPiece() >= 2; //si il y a deux pieces dans la même case c'est que c'est emboité
        }
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: pieceOver
    goal: retourne vrai si il y a une pièce au dessus ou emboitée
    preconditions: doit etre dans le plateau
    postconditions: boolean
    */
    private boolean pieceOver(Plateau pl) {
        return layerOver(pl) || isEmboitee(pl);
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: onBorder
    goal: renvoie vrai si la piece est située sur une des bordures du plateau
    preconditions: doit etre dans le plateau
    postconditions: boolean
    */
    private boolean onBorder(Plateau pl) {
        return this.getLowerX() > pl.getMinX() && this.getHigherX() < pl.getMaxX() && this.getLowerY() > pl.getMinY() && this.getHigherY() < pl.getMaxY();
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: isSurrounded
    goal: retourne vrai si la pièce est entourée dans le sens où on ne peut pas la déplacer
    preconditions: doit etre dans le palateau
    postconditions: boolean
    */
    private boolean isSurrounded(Plateau pl) {
        boolean res = true;
        //si c'est une pièce dont on peut faire la différence entre couchée et debout, et n'est pas sur un bord du plateau
        if (this.getTaille() != 1 && this.getTangage() == orientationTangage.HORIZONTAL && !this.onBorder(pl)) {
            for (Object bl : this.getCompo()) {

                Bloc b = (Bloc) bl;
                //check pour tous les blocs si ils sont entourés par 4 pieces (sur le plan horizontal)
                res = res && b.isSurrounded(pl);
            }
        } else {
            res = false;
        }
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: freePlace
    goal: renvoie vrai si la pièce peut loger là où elle est sans fusionner de manière incensée avec une autre pièce
    preconditions: doit etre dans plateau
    postconditions: boolean
    */
    private boolean freePlace(Plateau pl) {
        boolean res = true;
        for (Object bl : this.getCompo()) {

            Bloc b = (Bloc) bl;
            Case c = pl.getCase(b.getX(), b.getY(), b.getZ());
            res = res && (c.getContenu() == Case.formes.VIDE ||
                    b.getForme() == Bloc.formes.TRIANGLE && c.getContenu() == Case.formes.TRIANGLE && c.getOnTop().getPiece().canFitTogether(this));
        }

        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: goodOrientation
    goal: renvoie vrai si l'escalier est correctement orienté pour permettre le mouvement si on se déplace suivant les vecteurs vx, vy et vz
    preconditions: la pièce doit être un escalier, et les vecteurs des vecteurs unitaires (val abs = 1 ou 0 pr z)
    postconditions: boolean
    */
    public boolean goodOrientation(int vx, int vy, int vz) {
        int angle  = Math.floorMod((90 * (Math.abs(vy) + vx - vy + 2 * vz)), 360);
        return getRoulis() == angle;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: makeTuple
    goal: transforme les deux éléments en un tuple
    preconditions: $<none>$
    postconditions: linkedList, doit agir comme tel
    */
    private LinkedList makeTuple (int i, Case c) {
        LinkedList ret = new LinkedList();
        ret.add(c);
        ret.addFirst(i);
        return ret;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: moveCases
    goal: renvoie la liste des cases sur lesquelles peut aller un seigneur. Algo de croissance de région
    preconditions: la pièce doit être un seigneur
    postconditions: liste de cases
    */
    public LinkedList moveCases (Plateau pl) {
        LinkedList aTraiter = new LinkedList(), visited = new LinkedList();
        aTraiter.add(makeTuple(maxMove, pl.getCase(this.getX(), this.getY(), this.getZ())));
        while (!aTraiter.isEmpty()) {
            //on récupère le couple pm et case
            LinkedList o = (LinkedList)aTraiter.removeFirst();
            int pm = (int)o.getFirst();
            Case c = (Case)o.getLast();
            //si on peut encore se déplacer on établit toutes les cases voisines sur lesquelles on peut s'arreter
            if (pm > 0) {
                LinkedList voisins = pl.getVoisinsSeigneur(c);
//                System.out.println("appel get voisins seigneur, sors ça :" + voisins.toString());
                //on ajoute ds aTraier tous les voisins qui n'ont pas déjà été visités et qui ne sont pas encore à traiter
                for (Object v : voisins) {
                    Case cv = (Case) v;
                    if (!visited.contains(cv) && !aTraiter.contains(cv)) {aTraiter.add(makeTuple(pm - 1, cv));}
                }
            }
            //puis enfin on ajoute la case traitée aux cases déjà visitées
            visited.add(c);
            //comme toutes les cases visitées sont des cases où on peut s'arreter et que c'est ce que l'on cherche, on renvoie les cases visitées
        }
        return visited;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: canMove
    goal: renvoie 0 si la pièce peut être déplacée, un numéro correspond à l'erreur sinon
    preconditions: none
    postconditions: int représentant l'erreur si différent de 0
    */
    public int canMove(Plateau pl) {
        int error = 0; //on initialise l'erreur à 0. Tant que tout va bien elle y reste, c'est la valeur de retour.

        if (this.getName() == names.TOUR) {
            error = -1; //on n'a PAS le droit de déplacer la tour!
        } else {
            if (this.getName() != names.SEIGNEUR) {//le seigneur pourra toujours bouger
                //on check qu'il n'y a aucune pièce au dessus d'elle
                if (!this.inReserve()) {
                    if (this.pieceOver(pl)) {
                        error = -2; //on n'a pas le droit de déplacer si il y a une pièce au dessus ou emboitée
                    }
                }

                //on vérifie aussi qu'elle n'est pas entourée
                if (error == 0) {
                    if (this.isSurrounded(pl)) {
                        error = -3; //tu peux pas déplacer si elle est encerclée
                    }
                }
            }
        }
        return error;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: goalValideConstr
    goal: renvoie 0 si la piece DE CONSTRUCTION peut aller à l'endroit indiqué, un numéro correspondant à l'erreur sinon
    preconditions: la pièce doit être une pièce de construction
    postconditions: int désignant l'erreur
    */
    private int goalValideConstr(Plateau pl, int x, int y, int z, orientationLacet lct, orientationTangage tng, int rls) {
        int error = 0;

        //on fait une sauvegarde des données de la pièce
        orientationTangage tng_default = this.getTangage();
        orientationLacet lct_default = this.getLacet();
        int rls_default = this.getRoulis();
        int x_default = this.x;
        int y_default = this.y;
        int z_default = this.z;

        //on fait pivoter la pièce afin de pouvoir faire les calculs de positionnement etc
        this.setTangage(tng);
        if (this.getTangage() == orientationTangage.HORIZONTAL) {this.setLacet(lct);}
        this.setRoulis(rls);

        //on déplace la pièce uniquement sur ses coordonnées, pas sur le plateau
        //c'est un peu la même chose que lorsqu'on visualise où on voudrait mettre la pièce afin de savoir si elle loge ou non
        this.setX(x);
        this.setY(y);
        this.setZ(z);

        //on check que tout est ok

        //doit être dans le plateau
        if (!this.inPlateau(pl)) {
            error = 1;
        }

        if (error == 0) {
            if (this.isOverTower()) {
                //rien qui ne se trouve au dessus de la tour
                error = 2;
            }
        }

        if (error == 0) {
            if (this.voidUnder(pl)) {
                //rien qui ne se trouve dans le vide
                error = 3;
            }
        }

        if (error == 0) {
            if (!this.freePlace(pl)) {
                //vérifier aussi que dans toutes les cases que la pièce va occuper, il n'y a aucune autre pièce qui ne puisse cohabiter
                error = 4; //quelque chose occupe l'espace que la pièce voudrait occuper !
            }
        }

        //reset les stats de la pièce
        this.setZ(z_default);
        this.setY(y_default);
        this.setX(x_default);
        this.setRoulis(rls_default);
        if (this.getTangage() == orientationTangage.HORIZONTAL) {this.setLacet(lct_default);}
        this.setTangage(tng_default);

        return error;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: goalValide
    goal: renvoie 0 si la pièce peut loger aux coordonnées indiquée, un numéro correspondant à l'erreur sinon
    preconditions: none
    postconditions: int représentant l'erreur si différent de 0
    */
    public int goalValide(Plateau pl, int x, int y, int z, orientationLacet lct, orientationTangage tng, int rls) {
        int res = 0;
        if (this.getName() == names.SEIGNEUR) {
            LinkedList casesPossibles = this.moveCases(pl);
            //System.out.println(casesPossibles.toString());
            boolean okay = false;
            for (Object o : casesPossibles) {
                Case c = (Case)o;
                okay = okay || (c.getX() == x && c.getY() == y && c.getZ() == z);
            }
            if (!okay) {res = 5;} //le seigneur ne peut pas aller sur cette case
        } else {
            res = this.goalValideConstr(pl, x, y, z, lct, tng, rls);
        }
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: applyMove
    goal: applique le déplacement de la pièce dans la structure 
    preconditions: la pièce doit pouvoir se déplacer et la destination doit être valide
    postconditions: none
    */
    public void applyMove(Plateau pl, int x, int y, int z, orientationLacet lct, orientationTangage tng, int rls) {
        //on supprime la pièce de l'endroit où elle est dans le plateau si elle est sur le plateau
        if (!this.inReserve()) {pl.delPiece(this);}
        this.enReserve = false;

        //on fait pivoter la pièce
        this.setTangage(tng);
        if (this.getTangage() == orientationTangage.HORIZONTAL) {this.setLacet(lct);}
        this.setRoulis(rls);

        //on déplace la pièce uniquement sur ses coordonnées, pas sur le plateau
        this.setX(x);
        this.setY(y);
        this.setZ(z);

        //on le réenregistre dans le plateau
        pl.addPiece(this);
    }

    
    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: move
    goal: gère tout le déplacement des pièces, il suffit de dire comment elle sera orientée et quelle est la destination de la base
    preconditions: none
    postconditions: none
    */
    //pl est le plateau de jeu
    //le triplé x y z est la coordonnée à laquelle se situera la base de la pièce
    //et les trois orientations la position dans l'espace dans laquelle elle sera
    public int move(Plateau pl, int x, int y, int z, orientationLacet lct, orientationTangage tng, int rls) {
        int error = this.canMove(pl);
        if (error == 0) {
            error = this.goalValide(pl, x, y, z, lct, tng, rls);
            if (error == 0) {
                this.applyMove(pl, x, y, z, lct, tng, rls);
            } else  {
                System.out.println("error goal valide :"+error);
            }
        } else {
            System.out.println("error can move :"+error);
        }
        return error;
    }

    public int askMove(Plateau p) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("case destination");
        System.out.println("x:");
        int x = keyboard.nextInt();
        System.out.println("y:");
        int y = keyboard.nextInt();
        System.out.println("z:");
        int z = keyboard.nextInt();
        System.out.println("\nOrientation Lacet");
        System.out.println("1:NORD");
        System.out.println("2:EST");
        System.out.println("3:SUD");
        System.out.println("4:OUEST");
        int l = keyboard.nextInt();
        orientationLacet lct;
        switch (l) {
            case 1:
                lct = orientationLacet.NORD;
                break;
            case 2:
                lct = orientationLacet.EST;
                break;
            case 3:
                lct = orientationLacet.SUD;
                break;
            default:
                lct = orientationLacet.OUEST;
        }
        System.out.println("\nOrientation Tangage");
        System.out.println("1:HAUT");
        System.out.println("2:HORIZONTAL");
        System.out.println("3:BAS");
        int t = keyboard.nextInt();
        orientationTangage tg;
        switch (t) {
            case 1:
                tg = orientationTangage.HAUT;
                break;
            case 2:
                tg = orientationTangage.HORIZONTAL;
                break;
            default:
                tg = orientationTangage.BAS;
        }
        System.out.println("\nOrientation Roulis");
        System.out.println("0");
        System.out.println("90");
        System.out.println("180");
        System.out.println("270");
        int r = keyboard.nextInt();
        return move(p,x,y,z,lct,tg,r);
    }

}
