import java.lang.reflect.Array;
import java.util.Random;

//adn de l'ia, ce qui lui permet de réfléchir
public class Individu {
    private int[] poidsPieces; //dans l'ordre : bloc taille 3, 2, 1, puis escaliers même ordre
    final int minPoidsPiece = 1; //poids mini d'une piece
    final int maxPoidsPiece = 100; //poids maxi d'une pièce
    final int cote = 10;
    final int hauteur = 8; //on met 8 cases car la hauteur est de 4, décomposée en 2 (pour les 2 escaliers)
    private int[][][] poidsCases; // chaque case à un poids lié à son importance, taille = 10x10x8 = 600
    final int minPoidsCase = -10; //poids mini d'une case
    final int maxPoidsCase = 10; //poids maxi d'une case
    private int agroNoir; //agressivité si il est joueur noir
    private int agroBlanc; //même chose si il est blanc
    private int defNoir; //pareil pour la défense
    private int defBlanc;
    final int minTactique = 1; //poids mini pour l'aggro ou la def
    final int maxTactique = 100; //idem pour le max
    //la stratégie d'attaque et de défense varie en fonction qu'on est le premier ou le deuxième joueur
    private int coefDistanceTour; //donnera importance à la distance qu'il reste à parcourir jusqu'a la tour
    private int coefDistancePoss; //donnera importance à la distance qu'il peut parcourir actuellement vers la tour
    final int minDistance = 1; //min et max pour les coefficients de distance
    final int maxDistance = 50;
    private int score;
    private double proba;
    final double mut = 0.03; //probabilité de muter
    final double co = 0.5; //probabilité de commencer un nouveau cross-over

    public int myRandom(int min, int max) {
        Random rand = new Random();
        return rand.nextInt(max - min + 1) + min;
    }

    private int[][][] initPoidsCases() {
        int[][][] t = new int[cote][cote][hauteur];
        for (int x = 0; x < cote; x++){
            for(int y = 0; y < cote; y++) {
                for(int z = 0; z < hauteur; z++) {
                    t[x][y][z] = myRandom(minPoidsCase,maxPoidsCase); //on initialise toutes les cases au hasard
                }
            }
        }
        return t;
    }

    //crée un individu random
    public Individu() {
        poidsPieces = new int[]{myRandom(minPoidsPiece, maxPoidsPiece), //poids bloc taille 3
                                myRandom(minPoidsPiece, maxPoidsPiece), //poids bloc taille 2
                                myRandom(minPoidsPiece, maxPoidsPiece), //poids bloc taille 1
                                myRandom(minPoidsPiece, maxPoidsPiece), //poids escalier taille 3
                                myRandom(minPoidsPiece, maxPoidsPiece), //poids escalier taille 2
                                myRandom(minPoidsPiece, maxPoidsPiece)};//poids escalier taille 1
        poidsCases = initPoidsCases();
        agroNoir  = myRandom(minTactique,maxTactique);
        agroBlanc = myRandom(minTactique,maxTactique);
        defNoir   = myRandom(minTactique,maxTactique);
        defBlanc  = myRandom(minTactique,maxTactique);
        coefDistanceTour = myRandom(minDistance,maxDistance);
        coefDistancePoss = myRandom(minDistance,maxDistance);
        score = 0; //hé oui, au début il n'a pas de score
    }

    //crée une copie de l'individu
    public Individu (Individu i) {
        agroNoir  = i.getAgroNoir();
        agroBlanc = i.getAgroBlanc();
        defNoir   = i.getDefNoir();
        defBlanc  = i.getDefBlanc();
        coefDistanceTour = i.getCoefDistanceTour();
        coefDistancePoss = i.getCoefDistancePoss();
        poidsPieces = new int[6];
        poidsCases = new int[cote][cote][hauteur];
        for (int j = 0; j < this.poidsPieces.length; j++) {
            poidsPieces[j] = i.getPoidsPieces()[j];
        }
        for (int x = 0; x < cote; x++) {
            for (int y = 0; y < cote; y++) {
                for (int z = 0; z < hauteur; z++) {
                    poidsCases[x][y][z] = i.getPoidsCases()[x][y][z];
                }
            }
        }
        score = i.getScore();
        proba = i.getProba();
    }

    //***************** ACCESSEURS *****************
    private String poidsPiecesToString () {
        String res = "[";
        for(int val: getPoidsPieces()) {
            res += val + ",";
        }
        return res + "]";
    }

    private String poidsCasesToString () {
        String res = "\n";
        for (int y = hauteur - 1; y >= 0; y--) {
            for (int x = 0; x < hauteur; x++) {
                res += "[";
                for (int z = 0; z < hauteur; z++) {
                    res += + getPoidsCases()[x][y][z] + ((z == 2)? "" : "," );
                }
                res += "]";
            }
            res += "\n";
        }
        return res;

    }

    @Override
    public String toString() {
        return "poidsPieces : " + poidsPiecesToString() + "\n" +
        "poidsCases : " + poidsCasesToString() +
        "agroNoir : " + agroNoir + "\n" +
        "agroBlanc : " + agroBlanc + "\n" +
        "defNoir : " + defNoir + "\n" +
        "defBlanc : " + defBlanc + "\n" +
        "coefDistanceTour : " + coefDistanceTour + "\n" +
        "coefDistancePoss : " + coefDistancePoss + "\n" +
        "score : " + score;

    }

    public int[] getPoidsPieces() {
        return poidsPieces;
    }

    public int[][][] getPoidsCases() {
        return poidsCases;
    }

    public int getAgroNoir() {
        return agroNoir;
    }

    public int getAgroBlanc() {
        return agroBlanc;
    }

    public int getDefNoir() {
        return defNoir;
    }

    public int getDefBlanc() {
        return defBlanc;
    }

    public int getCoefDistanceTour() {
        return coefDistanceTour;
    }

    public int getCoefDistancePoss() {
        return coefDistancePoss;
    }

    public int getScore() {
        return score;
    }

    public double getProba() {
        return proba;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setProba(double proba) {
        this.proba = proba;
    }

    private int muterGene (int borneMin, int borneMax, int valGene, double coefMut) {
        int res;
        int variance = (int)Math.round((borneMax - borneMin + 1) * coefMut);
        res = myRandom(valGene - variance, valGene + variance);
        if (res < borneMin) {res = borneMin;}
        if (borneMax < res) {res = borneMax;}
        return res;
    }

    //fait muter un individu
    public void mutation() {
        double k = 0.3; //permet d'ajuster en fonction de l'ordre de grandeur, pour que les mutations varies sans être poussées vers bornes
        double coefMutation = 1 / (this.getProba() * Math.abs(this.getScore()) * k + 1.0); //doit être < 1
        if (Math.random() < mut) {this.agroNoir = muterGene(minTactique,maxTactique,getAgroNoir(),coefMutation);}
        if (Math.random() < mut) {this.agroBlanc = muterGene(minTactique,maxTactique,getAgroBlanc(),coefMutation);}
        if (Math.random() < mut) {this.defNoir = muterGene(minTactique,maxTactique,getDefNoir(),coefMutation);}
        if (Math.random() < mut) {this.defBlanc = muterGene(minTactique,maxTactique,getDefBlanc(),coefMutation);}
        if (Math.random() < mut) {this.coefDistancePoss = muterGene(minDistance,maxDistance,getCoefDistancePoss(),coefMutation);}
        if (Math.random() < mut) {this.coefDistanceTour = muterGene(minDistance,maxDistance,getCoefDistanceTour(),coefMutation);}
        for (int i = 0; i < this.poidsPieces.length; i++) {
            if (Math.random() < mut) {this.poidsPieces[i] = muterGene(minTactique,maxTactique,getPoidsPieces()[i],coefMutation);}
        }
        for (int x = 0; x < cote; x++) {
            for (int y = 0; y < cote; y++) {
                for (int z = 0; z < hauteur; z++) {
                    if (Math.random() < mut) {this.poidsCases[x][y][z] = muterGene(minTactique,maxTactique,getPoidsCases()[x][y][z],coefMutation);}
                }
            }
        }
    }

    //reproduit cet individu avec l'autre, renvoie deux enfants
    public Individu[] reproduction (Individu other) {
        Individu[] child = new Individu[2];
        child[0] = new Individu(); //on crée deux individus, on va mettre leurs valeurs après
        child[1] = new Individu();
        if (Math.random() < co) {
            child[0].agroNoir = this.getAgroNoir();
            child[1].agroNoir = other.getAgroNoir();
        } else {
            child[1].agroNoir = this.getAgroNoir();
            child[0].agroNoir = other.getAgroNoir();
        }
        if (Math.random() < co) {
            child[0].agroBlanc = this.getAgroBlanc();
            child[1].agroBlanc = other.getAgroBlanc();
        } else {
            child[1].agroBlanc = this.getAgroBlanc();
            child[0].agroBlanc = other.getAgroBlanc();

        }
        if (Math.random() < co) {
            child[0].defNoir = this.getDefNoir();
            child[1].defNoir = other.getDefNoir();
        } else {
            child[1].defNoir = this.getDefNoir();
            child[0].defNoir = other.getDefNoir();

        }
        if (Math.random() < co) {
            child[0].defBlanc = this.getDefBlanc();
            child[1].defBlanc = other.getDefBlanc();
        } else {
            child[1].defBlanc = this.getDefBlanc();
            child[0].defBlanc = other.getDefBlanc();

        }
        if (Math.random() < co) {
            child[0].coefDistancePoss = this.getCoefDistancePoss();
            child[1].coefDistancePoss = other.getCoefDistancePoss();
        } else {
            child[1].coefDistancePoss = this.getCoefDistancePoss();
            child[0].coefDistancePoss = other.getCoefDistancePoss();

        }
        if (Math.random() < co) {
            child[0].coefDistanceTour = this.getCoefDistanceTour();
            child[1].coefDistanceTour = other.getCoefDistanceTour();
        } else {
            child[1].coefDistanceTour = this.getCoefDistanceTour();
            child[0].coefDistanceTour = other.getCoefDistanceTour();
        }
        for (int i = 0; i < this.poidsPieces.length; i++) {
            if (Math.random() < co) {
                child[0].poidsPieces[i] = this.getPoidsPieces()[i];
                child[1].poidsPieces[i] = other.getPoidsPieces()[i];
            } else {
                child[1].poidsPieces[i] = this.getPoidsPieces()[i];
                child[0].poidsPieces[i] = other.getPoidsPieces()[i];
            }
        }
        for (int x = 0; x < cote; x++) {
            for (int y = 0; y < cote; y++) {
                for (int z = 0; z < hauteur; z++) {
                    if (Math.random() < co) {
                        child[0].poidsCases[x][y][z] = this.getPoidsCases()[x][y][z];
                        child[1].poidsCases[x][y][z] = other.getPoidsCases()[x][y][z];
                    } else {
                        child[1].poidsCases[x][y][z] = this.getPoidsCases()[x][y][z];
                        child[0].poidsCases[x][y][z] = other.getPoidsCases()[x][y][z];
                    }
                }
            }
        }
        int scMoy = (this.getScore() + other.getScore()) /2;
        double prMoy = (this.getProba() + other.getProba()) /2;
        child[0].setScore(scMoy);
        child[1].setScore(scMoy);
        child[0].setProba(prMoy);
        child[1].setProba(prMoy);
        return child;

    }
}
