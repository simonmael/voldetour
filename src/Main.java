import java.util.LinkedList;
import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        test();
        //launchGame();
    }

    private static void test() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("charger à partir d'une population déjà existante? (y/n)");
        boolean load = (keyboard.next() == "y");
        if (load) { System.out.println("fichier : ");}
        Population p = (load) ? new Population(keyboard.next()) : new Population() ;
        p.evolution();
    }

    private static void launchGame() {
        Joueur j1 = new Joueur(Piece.couleurs.NOIR,"joueur 1");
        Joueur j2 = new Joueur(Piece.couleurs.BLANC,"joueur 2");
        Plateau plateau = new Plateau(j1,j2);
        tests(plateau,j1,j2);
        //jouer(plateau,j1,j2);
    }

    public static void jouer (Plateau p, Joueur j1, Joueur j2) {
        while (!p.endGame()) {
            j1.tour(p,j2,false);
            if (!p.endGame()) {
                j2.tour(p, j1, false);
                p.tourSuivant();
            }
        }
        System.out.println("Partie finie. \n" + ((p.victory(Piece.couleurs.NOIR)) ? "noir" : "blanc") + " a gagné");
    }


    private static void tests (Plateau pl, Joueur j1, Joueur j2) {
        //j1
        Piece p = (Piece) j1.getPiece(Piece.names.ESCALIER, 3).getFirst();
        System.out.println("1");
        p.move(pl, 3, 2, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 270);
        p = (Piece) j1.getPiece(Piece.names.BLOC, 2).getFirst();
        System.out.println("2");
        p.move(pl, 4, 2, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        p = (Piece) j1.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        System.out.println("3");
        p.move(pl, 4, 2, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        //j2
        p = (Piece) j2.getPiece(Piece.names.ESCALIER, 1).getFirst();
        System.out.println("4");
        p.move(pl, 3, 2, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.BAS, 270);
        //fin tour 1
        pl.tourSuivant();
        //j1
        p = (Piece) j1.getPiece(Piece.names.ESCALIER, 2).getFirst();
        System.out.println("5");
        p.move(pl, 5, 2, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 270);
        p = (Piece) j1.getPiece(Piece.names.BLOC, 2).getFirst(); //normalement on est sur lui
        System.out.println("la il doit gueuler");
        p.move(pl, 6, 2, 0, Piece.orientationLacet.EST, Piece.orientationTangage.HORIZONTAL, 0);
        p = (Piece) j1.getPiece(Piece.names.BLOC, 2).getLast();
        System.out.println("6");
        p.move(pl, 6, 2, 0, Piece.orientationLacet.EST, Piece.orientationTangage.HORIZONTAL, 0);
        p = (Piece) j1.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        System.out.println("7");
        //System.out.println(pl.toString());
        p.move(pl, 7, 2, 1, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        //j2
        System.out.println("on va essayer de faire flotter une pièce");
        p = (Piece) j2.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        p.move(pl, 6, 7, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        p = (Piece) j2.getPiece(Piece.names.ESCALIER, 3).getFirst();
        System.out.println("8");
        p.move(pl, 6, 7, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 90);
        System.out.println("on va essayer de faire tenir en equilibre le seigneur sur l'escalier");
        p = (Piece) j2.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        p.move(pl, 6, 7, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        //fin tour 2
        pl.tourSuivant();
        //j1
        p = (Piece) j1.getPiece(Piece.names.ESCALIER, 2).getFirst();
        System.out.println("9");
        p.move(pl, 7, 3, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        p = (Piece) j1.getPiece(Piece.names.BLOC, 2).getFirst();
        System.out.println("10");
        p.move(pl, 7, 4, 0, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        p = (Piece) j1.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        System.out.println("on fait tenir le seigneur sur l'escalier");
        p.move(pl, 7, 3, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        System.out.println("11");
        p.move(pl, 7, 4, 2, Piece.orientationLacet.NORD, Piece.orientationTangage.HAUT, 0);
        //j2
        System.out.println("on emboite deux pièces !");
        p = (Piece) j2.getPiece(Piece.names.ESCALIER, 2).getFirst();
        p.move(pl, 7, 2, 1, Piece.orientationLacet.NORD, Piece.orientationTangage.HORIZONTAL, 180);
        //fin tour 3
        System.out.println("victoire avant tour:" + pl.endGame());
    }

}

