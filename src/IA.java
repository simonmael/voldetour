import jdk.nashorn.internal.scripts.JO;
import sun.awt.image.ImageWatched;

import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarOutputStream;

//IA du jeu
public class IA {
    final int VICTOIRE = 100000;
    final int DEFAITE = - VICTOIRE;
    private Individu genome; //le génome de l'IA


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: IA
    goal: crée l'ia
    preconditions: g le génome de l'IA
    postconditions: none
    */
    public IA (Individu g) {
        genome = g;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: IA
    goal: crée une ia au hasard
    preconditions: none
    postconditions: none
    */
    public IA () {
        genome = new Individu();
    }



    //***************** ACCESSEURS AU GENOME *****************

    public Individu getGenome() {
        return genome;
    }

    public int getPoidsPiece(int n) {
        return getGenome().getPoidsPieces()[n];
    }

    public int[][][] getPoidsCases() {
        return getGenome().getPoidsCases();
    }

    public int getAgroNoir() {
        return getGenome().getAgroNoir();
    }

    public int getAgroBlanc() {
        return getGenome().getAgroBlanc();
    }

    public int getDefNoir() {
        return getGenome().getDefNoir();
    }

    public int getDefBlanc() {
        return getGenome().getDefBlanc();
    }

    public int getCoefDistanceTour() {
        return getGenome().getCoefDistanceTour();
    }

    public int getCoefDistancePoss() {
        return getGenome().getCoefDistancePoss();
    }


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: canMoveOnBoard
    goal: renvoie toutes les pièces qui peuvent bouger & qui sont sur le plateau
    preconditions: none
    postconditions: la liste des pièces sur le plateau qui peuvent bouger
    */
    private LinkedList<Piece> canMoveOnBoard (Plateau pl, Joueur me) {
        LinkedList<Piece> cm = me.getDispos(pl);
        LinkedList<Piece> ret = new LinkedList<>();
        for (Piece p : cm) {
            if (!p.inReserve()) {
                ret.add(p);
            }
        }
        return ret;
    }



    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getControles
    goal: renvoie la liste de toutes les pièces qui sont sous le controle du joueur me
    preconditions: none
    postconditions: liste de toutes les pièces sous le controle du joueur me
    */
    private LinkedList<Piece> getControles (Plateau pl, Joueur me) {
        LinkedList<Piece> aTraiter = canMoveOnBoard(pl, me);
        LinkedList<Piece> visited = new LinkedList<>();
        while (!aTraiter.isEmpty()) { //croissance de région en gros
            //on récupère la première pièce
            Piece p = aTraiter.getFirst();
            for (Bloc b : p.getCompo()) {
                Piece pv;
                int x = b.getX();
                int y = b.getY();
                int z = b.getZ();
                if (z > 0) {//on récupère la pièce qui est dans la case sous le bloc
                    pv = pl.getCase(x,y,z - 1).getOnTop().getPiece();
                    if (!aTraiter.contains(pv) && !visited.contains(pv)) {
                        aTraiter.add(pv);
                    }
                }//ici c'est si on est un escalier qui est sur un autre qu'on peut ajouter, sinon ça ne fera rien
                pv = pl.getCase(x,y,z).getOnTop().getPiece();
                if (!aTraiter.contains(pv) && !visited.contains(pv)) {
                    aTraiter.add(pv);
                }
                aTraiter.removeFirst();
                visited.add(p);
            }
        }
        return visited;
    }



    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: casesControlees
    goal: renvoie le poids associé aux cases sous le controle du joueur passé en paramètres
    preconditions: none
    postconditions: integer
    */
    private int casesControlees(Plateau pl, Joueur me) {
        LinkedList<Piece> controles = getControles(pl,me);
        int score  = 0;
        for (Piece p : controles) {
            for (Bloc b : p.getCompo()) {
                int x = b.getX();
                int y = b.getY();
                int z = b.getZ();
                Case c = pl.getCase(x,y,z);
                if (c.getOnTop() == b) {
                    score += getPoidsCases()[x][y][z*2 + 1];
                }
                if (c.getBottom() == b) {
                    score += getPoidsCases()[x][y][z*2];
                }
            }
        }
        return score;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: combinePoidsPiece
    goal: fait la somme des poids des pièces présentes dans la liste
    preconditions: liste de pièces
    postconditions: int
    */
    private int combinePoidsPiece(LinkedList<Piece> pieces) {
        int res = 0;
        for (Piece p : pieces) {
            res += this.getPoidsPiece(p.getNumero());
        }
        return res;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: score_defensif
    goal: renvoie le score associé à la défense du joueur, plus il ralentit son adversaire en gros
    preconditions: none
    postconditions: int, plus il sera grand meilleur sea le score de cette personne
    */
    private int score_defensif (Plateau p, Joueur me, Joueur adv) {
            LinkedList<Piece> bloques = adv.getCoinces(p);
            int sControle = casesControlees(p,me);
            Piece sAdv = (Piece)adv.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
            int dist = getCoefDistanceTour() * distRest(sAdv);
            int poss = getCoefDistancePoss() * sAdv.moveCases(p).size();
            int sCoin = combinePoidsPiece(bloques);
        return sControle + dist + poss + sCoin;
    }

    private int distRest(Piece s) {
        int xygoal = (s.getCouleur() == Piece.couleurs.NOIR) ? 7 : 2;
        return Math.abs(xygoal - s.getX()) + Math.abs(xygoal - s.getY()) + Math.abs(3 - s.getZ());
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: score_offensif
    goal: renvoie le score associé à l'agressivité du joueur, plus il est proche de gagner la partie en gros
    preconditions: none
    postconditions: int, plus il sera grand meilleur sea le score de cette personne
    */
    private int score_offensif (Plateau p, Joueur me, Joueur adv) {
        Piece seigneur = (Piece)me.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        int dist = getCoefDistanceTour() * distRest(seigneur);
        int poss = getCoefDistancePoss() * seigneur.moveCases(p).size();
        return dist + poss;
    }


    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: possibilites
    goal: renvoie le score associé aux possibilités que l'on à grace aux pièces libres etc
    preconditions: none
    postconditions: int, plus il sera grand meilleur sera le score de cette personne
    */
    private int possibilites (Plateau p, Joueur me, Joueur adv) {
        LinkedList<Piece> dispos = me.getDispos(p);
        LinkedList<Piece> imuns  = me.getImuns(p, dispos);
        Piece seigneur = (Piece)me.getPiece(Piece.names.SEIGNEUR, 1).getFirst();
        int poss = getCoefDistancePoss() * seigneur.moveCases(p).size();
        int sDisp = combinePoidsPiece(dispos);
        int sImun = combinePoidsPiece(imuns);
        return poss + sDisp + sImun;
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: evaluate
    goal: renvoie un score associé au plateau dans l'état actuel
    preconditions: none
    postconditions: l'int renvoyé sera compris entre les constantes VICTOIRE et DEFAITE. Plus l'int sera grand, meilleur sera le plateau pour ce joueur
    */
    private int evaluate (Plateau p, Joueur me, Joueur adv) {
        return ((me.getCouleur() == Piece.couleurs.NOIR)? getAgroNoir(): getAgroBlanc() ) * score_offensif(p,me,adv)
               + ((me.getCouleur() == Piece.couleurs.NOIR)? getDefNoir(): getDefBlanc() ) * score_defensif(p,me,adv)
                + possibilites(p,me,adv);
    }










    

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: generateMoves
    goal: génère toutes les actions qui seront essayées dans l'algo minmax
    preconditions: none
    postconditions: none
    */
    private LinkedList<LinkedList<Action>> generateMoves (Plateau pl, Joueur j) {
        return generateMovesR(pl, j, new LinkedList<>(), new LinkedList<>());
    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: minimax
    goal: algorithme minimax qui renvoie la suite d'actions que l'IA va jouer
    preconditions: none
    postconditions: LinkedList avec le 1er élément qui est le score de l'action, et le reste de la liste qui est des actions
    */
    private LinkedList minimax (int depth, boolean myTurn, int alpha, int beta, Plateau p, Joueur me, Joueur adv) {
        LinkedList<LinkedList<Action>> nextMoves = generateMoves(p,(myTurn)? me : adv);

        int score;
        LinkedList actions = new LinkedList();

        if (nextMoves.isEmpty() || depth == 0) {
            // Gameover or depth reached, evaluate score
            score = evaluate(p, me, adv);
            actions.addFirst(score);
            return actions;
        } else {
            for (LinkedList<Action> act : nextMoves) {
                // try this move for the current "player"
                p.apply(act);
                if (myTurn) {  //tour de l'IA
                    score = (int)minimax(depth - 1, !myTurn, alpha, beta, p, me, adv).getFirst();
                    if (score > alpha) {
                        alpha = score;
                        actions = act;
                    }
                } else {  // oppSeed is minimizing player
                    score = (int)minimax(depth - 1, !myTurn, alpha, beta, p, me, adv).getFirst();
                    if (score < beta) {
                        beta = score;
                        actions = act;
                    }
                }
                // undo move
                p.undo(act);
                // cut-off
                if (alpha >= beta) break;
            }
            LinkedList ret = new LinkedList();
            ret.add((myTurn) ? alpha : beta);
            ret.add(actions);
            return ret;
        }

    }

    /*
    author: Mael Simon <simonmael@eisti.eu>
    name: getActions
    goal: renvoie les actions qu'aurait joué l'IA
    preconditions: none
    postconditions: none
    */
    public LinkedList getActions(Plateau p, Joueur me, Joueur adv) {
        LinkedList res = minimax(4, true, Integer.MIN_VALUE, Integer.MAX_VALUE, p, me, adv);
        res.removeFirst();
        return res;
    }
}
